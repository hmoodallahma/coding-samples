from django.test import TestCase
from ..models import Results, Paper


class ResultsTest(TestCase):
    """ Test module for Results model """

    def setUp(self):
        Results.objects.create(
            title='etf results', description='Lorem ipsum', paper=Paper.objects.create(title='asdasd'))
        Results.objects.create(
            title='bond results', description='Lorem ipsum', paper=Paper.objects.create(title='asdasd'))

    def tearDown(self):
        pass

    def test_results_title(self):
        """Test adding research results"""
        results_etf = Results.objects.get(title='etf results')
        results_bond = Results.objects.get(title='bond results')
        self.assertEqual(
            results_etf.get_title(), "Results: etf results")
        self.assertEqual(
            results_bond.get_title(), "Results: bond results")
