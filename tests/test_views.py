import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Hypothesis, Paper
from ..serializers import HypothesisSerializer
from django.contrib.auth.models import User



client = Client()


class GetAllHypothesesTest(TestCase):
	""" Test module for GET all hypotheses API """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		Hypothesis.objects.create(title='Casper', description='Black')
		Hypothesis.objects.create(title='Muffin',description='Brown')
		Hypothesis.objects.create(title='Rambo', description='Black')
		Hypothesis.objects.create(title='Ricky', description='Brown')

	def test_get_all_hypotheses(self):
	# get API response
		response = client.get(reverse('get_post_hypothesis'))
		# get data from db
		hypotheses = Hypothesis.objects.all()
		serializer = HypothesisSerializer(hypotheses, many=True)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleHypothesisTest(TestCase):
	""" Test module for GET single hypothesis API """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Hypothesis.objects.create(title='Casper', description='Black')
		self.cuffin = Hypothesis.objects.create(title='Muffin',description='Brown')
		self.cambo = Hypothesis.objects.create(title='Rambo', description='Black')
		self.cicky = Hypothesis.objects.create(title='Ricky', description='Brown')

	def test_get_valid_single_hypothesis(self):
		response = client.get(reverse('get_delete_update_hypothesis', kwargs={'pk': self.cambo.pk}))
		hypothesis = Hypothesis.objects.get(pk=self.cambo.pk)
		serializer = HypothesisSerializer(hypothesis)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_get_invalid_single_hypothesis(self):
		response = client.get(reverse('get_delete_update_hypothesis', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewHypothesisTest(TestCase):
	""" Test module for inserting a new puppy """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.valid_payload = {
			'title': 'Muffin',
			'description': 'lorem ipsum'
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem ipsum'
		}

	def test_create_valid_hypothesis(self):
		response = client.post(reverse('get_post_hypothesis'),data=json.dumps(self.valid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_invalid_hypothesis(self):
		response = client.post(reverse('get_post_hypothesis'),data=json.dumps(self.invalid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleHypothesisTest(TestCase):
	""" Test module for updating an existing hypothesis"""

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Hypothesis.objects.create(title='Casper', description='Black')
		self.cuffin = Hypothesis.objects.create(title='Muffin',description='Brown')

		self.valid_payload = {
			'title': 'Buffy',
			'description': 'ssssBlack'
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem'
		}

	def test_valid_update_hypothesis(self):
		response = client.put(reverse('get_delete_update_hypothesis',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.valid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_update_hypothesis(self):
		response = client.put(reverse('get_delete_update_hypothesis',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.invalid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleHypothesisTest(TestCase):
	""" Test module for deleting an existing hypothesis record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Hypothesis.objects.create(
		title='Casper', description='Black')
		self.muffin = Hypothesis.objects.create(
		title='Muffy', description='Brown')

	def test_valid_delete_hypothesis(self):
		response = client.delete(
		reverse('get_delete_update_hypothesis', kwargs={'pk': self.muffin.pk}))
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_delete_hypothesis(self):
		response = client.delete(
		reverse('get_delete_update_hypothesis', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)