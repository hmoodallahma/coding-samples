import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Results, Paper
from ..serializers import ResultsSerializer
from django.contrib.auth.models import User


# user = User.objects.create(username='aisha', password='bobrules')
client = Client()

class GetAllResultsTest(TestCase):
	""" Test module for GET all results API """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		Results.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdsa'))
		Results.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdsa'))
		Results.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='asdsa'))
		Results.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='asdsa'))

	def test_get_all_results(self):
	# get API response
		response = client.get(reverse('get_post_results'))
		# get data from db
		results = Results.objects.all()
		serializer = ResultsSerializer(results, many=True)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleResultsTest(TestCase):
	""" Test module for GET single results API """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Results.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdsa'))
		self.cuffin = Results.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdsa'))
		self.cambo = Results.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='asdsa'))
		self.cicky = Results.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='asdsa'))

	def test_get_valid_single_results(self):
		response = client.get(reverse('get_delete_update_results', kwargs={'pk': self.cambo.pk}))
		results = Results.objects.get(pk=self.cambo.pk)
		serializer = ResultsSerializer(results)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_get_invalid_single_results(self):
		response = client.get(reverse('get_delete_update_results', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewResultsTest(TestCase):
	"""Test module for inserting a new results"""
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.valid_payload = {
			'title': 'Muffin',
			'description': 'lorem ipsum',
			'paper': Paper.objects.create(title='sdfsdf').id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem ipsum'
		}

	def test_create_valid_results(self):
		response = client.post(reverse('get_post_results'),data=json.dumps(self.valid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_invalid_results(self):
		response = client.post(reverse('get_post_results'),data=json.dumps(self.invalid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleResultsTest(TestCase):
	""" Test module for updating an existing puppy record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Results.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdsa'))
		self.cuffin = Results.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdsa'))

		self.valid_payload = {
			'title': 'Buffy',
			'description': 'ssssBlack',
			'paper': self.cuffin.paper.id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem'
		}

	def test_valid_update_results(self):
		response = client.put(reverse('get_delete_update_results',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.valid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_update_results(self):
		response = client.put(reverse('get_delete_update_results',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.invalid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleResultsTest(TestCase):
	""" Test module for deleting an existing results record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Results.objects.create(
		title='Casper', description='Black', paper=Paper.objects.create(title='asdsa'))
		self.muffin = Results.objects.create(
		title='Muffy', description='Brown', paper=Paper.objects.create(title='asdsa'))

	def test_valid_delete_results(self):
		response = client.delete(
		reverse('get_delete_update_results', kwargs={'pk': self.muffin.pk}))
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_delete_results(self):
		response = client.delete(
		reverse('get_delete_update_results', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)