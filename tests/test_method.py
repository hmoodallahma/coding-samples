from django.test import TestCase
from ..models import Method, Paper


class MethodTest(TestCase):
    """ Test module for Method model """

    def setUp(self):
        paper = Paper.objects.create(title='sdfsdf')
        paper1 = Paper.objects.create(title='sdfsdf1')
        Method.objects.create(
            title='etf method', description='Lorem ipsum', paper=paper)
        Method.objects.create(
            title='bond method', description='Lorem ipsum', paper=paper1)

    def tearDown(self):
        pass

    def test_method_title(self):
        method_etf = Method.objects.get(title='etf method')
        method_bond = Method.objects.get(title='bond method')
        self.assertEqual(
            method_etf.get_title(), "Method: etf method")
        self.assertEqual(
            method_bond.get_title(), "Method: bond method")
