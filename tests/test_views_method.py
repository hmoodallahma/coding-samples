import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Method, Paper
from ..serializers import MethodSerializer
from django.contrib.auth.models import User


# initialize the APIClient app
# user = User.objects.create(username='aisha', password='bobrules')
client = Client()


class GetAllMethodsTest(TestCase):
	""" Test module for GET all methods API """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		Method.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		Method.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdasd'))
		Method.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='asdasd'))
		Method.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='asdasd'))

	def test_get_all_methods(self):
	# get API response
		response = client.get(reverse('get_post_method'))
		# get data from db
		methods = Method.objects.all()
		serializer = MethodSerializer(methods, many=True)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleMethodTest(TestCase):
	""" Test module for GET single method API """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Method.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.cuffin = Method.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdasd'))
		self.cambo = Method.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.cicky = Method.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='asdasd'))

	def test_get_valid_single_method(self):
		response = client.get(reverse('get_delete_update_method', kwargs={'pk': self.cambo.pk}))
		method = Method.objects.get(pk=self.cambo.pk)
		serializer = MethodSerializer(method)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_get_invalid_single_method(self):
		response = client.get(reverse('get_delete_update_method', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewMethodTest(TestCase):
	""" Test module for inserting a new methods """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.valid_payload = {
			'title': 'Muffin',
			'description': 'lorem ipsum',
			'paper': Paper.objects.create(title='sdfsdf').id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem ipsum'
		}

	def test_create_valid_method(self):
		response = client.post(reverse('get_post_method'),data=json.dumps(self.valid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_invalid_method(self):
		response = client.post(reverse('get_post_method'),data=json.dumps(self.invalid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleMethodTest(TestCase):
	""" Test module for updating an existing method record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Method.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.cuffin = Method.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdasd'))

		self.valid_payload = {
			'title': 'Buffy',
			'description': 'ssssBlack',
			'paper': self.cuffin.paper.id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem'
		}

	def test_valid_update_method(self):
		response = client.put(reverse('get_delete_update_method',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.valid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_update_method(self):
		response = client.put(reverse('get_delete_update_method',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.invalid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleMethodTest(TestCase):
	""" Test module for deleting an existing method record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Method.objects.create(
		title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.muffin = Method.objects.create(
		title='Muffy', description='Brown', paper=Paper.objects.create(title='asdasd'))

	def test_valid_delete_method(self):
		response = client.delete(
		reverse('get_delete_update_method', kwargs={'pk': self.muffin.pk}))
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_delete_method(self):
		response = client.delete(
		reverse('get_delete_update_method', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)