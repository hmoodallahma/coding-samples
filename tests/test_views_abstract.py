import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Abstract, Paper
from ..serializers import AbstractSerializer
from django.contrib.auth.models import User


# initialize the APIClient app
# user = User.objects.create(username='aisha', password='bobrules')
client = Client()



class GetAllAbstractsTest(TestCase):
	""" Test module for GET all abstract API """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		Abstract.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		Abstract.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdasd'))
		Abstract.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='asdasd'))
		Abstract.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='asdasd'))

	def tearDown(self):
		pass

	def test_get_all_abstracts(self):
	# get API response
		response = client.get(reverse('get_post_abstract'))
		# get data from db
		abstracts = Abstract.objects.all()
		serializer = AbstractSerializer(abstracts, many=True)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleAbstractTest(TestCase):
	""" Test module for GET single abstract API """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Abstract.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.cuffin = Abstract.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdasd'))
		self.cambo = Abstract.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.cicky = Abstract.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='asdasd'))

	def tearDown(self):
		pass

	def test_get_valid_single_abstract(self):
		response = client.get(reverse('get_delete_update_abstract', kwargs={'pk': self.cambo.pk}))
		abstract = Abstract.objects.get(pk=self.cambo.pk)
		serializer = AbstractSerializer(abstract)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_get_invalid_single_abstract(self):
		response = client.get(reverse('get_delete_update_abstract', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewAbstractTest(TestCase):
	""" Test module for inserting a new abstract """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.valid_payload = {
			'title': 'Muffin',
			'description': 'lorem ipsum',
			'paper': Paper.objects.create(title='sdfsdf').id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem ipsum'
		}
	def tearDown(self):
		pass
	def test_create_valid_abstract(self):
		response = client.post(reverse('get_post_abstract'),data=json.dumps(self.valid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_invalid_abstract(self):
		response = client.post(reverse('get_post_abstract'),data=json.dumps(self.invalid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleAbstractTest(TestCase):
	""" Test module for updating an existing puppy record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Abstract.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.cuffin = Abstract.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdasd'))

		self.valid_payload = {
			'title': 'Buffy',
			'description': 'ssssBlack',
			'paper': self.cuffin.paper.id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem'
		}

	def test_valid_update_abstract(self):
		response = client.put(reverse('get_delete_update_abstract',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.valid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_update_abstract(self):
		response = client.put(reverse('get_delete_update_abstract',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.invalid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleAbstractTest(TestCase):
	""" Test module for deleting an existing abstract record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Abstract.objects.create(
		title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.muffin = Abstract.objects.create(
		title='Muffy', description='Brown', paper=Paper.objects.create(title='asdasd'))

	def test_valid_delete_abstract(self):
		response = client.delete(
		reverse('get_delete_update_abstract', kwargs={'pk': self.muffin.pk}))
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_delete_abstract(self):
		response = client.delete(
		reverse('get_delete_update_abstract', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)