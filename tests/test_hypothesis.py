from django.test import TestCase
from ..models import Hypothesis, Paper


class HypothesisTest(TestCase):
    """ Test module for Hypothesis model """

    def setUp(self):
        Hypothesis.objects.create(
            title='etf hypothesis', description='Lorem ipsum')
        Hypothesis.objects.create(
            title='bond hypothesis', description='Lorem ipsum')

    def tearDown(self):
        pass

    def test_hypothesis_title(self):
        hypothesis_etf = Hypothesis.objects.get(title='etf hypothesis')
        hypothesis_bond = Hypothesis.objects.get(title='bond hypothesis')
        self.assertEqual(
            hypothesis_etf.get_title(), "Hypothesis: etf hypothesis")
        self.assertEqual(
            hypothesis_bond.get_title(), "Hypothesis: bond hypothesis")

