from django.test import TestCase
from ..models import Research, Paper


class ResearchTest(TestCase):
    """ Test module for Research model """

    def setUp(self):
        paper = Paper.objects.create(title='sdfsdf')
        paper1 = Paper.objects.create(title='sdfsdf1')
        Research.objects.create(
            title='etf research', description='Lorem ipsum', paper=paper)
        Research.objects.create(
            title='bond research', description='Lorem ipsum', paper=paper1)

    def tearDown(self):
        pass

    def test_research_title(self):
        """Test addind research titles"""
        research_etf = Research.objects.get(title='etf research')
        research_bond = Research.objects.get(title='bond research')
        self.assertEqual(
            research_etf.get_title(), "Research: etf research")
        self.assertEqual(
            research_bond.get_title(), "Research: bond research")
