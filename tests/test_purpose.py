from django.test import TestCase
from ..models import Purpose, Paper


class PurposeTest(TestCase):
    """ Test module for Purpose model """

    def setUp(self):
        Purpose.objects.create(
            title='etf purpose', description='Lorem ipsum', paper=Paper.objects.create(title='asdsa'))
        Purpose.objects.create(
            title='bond purpose', description='Lorem ipsum', paper=Paper.objects.create(title='asdsa'))
    def tearDown(self):
        pass

    def test_purpose_title(self):
        """Test adding research purpose """
        purpose_etf = Purpose.objects.get(title='etf purpose')
        purpose_bond = Purpose.objects.get(title='bond purpose')
        self.assertEqual(
            purpose_etf.get_title(), "Purpose: etf purpose")
        self.assertEqual(
            purpose_bond.get_title(), "Purpose: bond purpose")
