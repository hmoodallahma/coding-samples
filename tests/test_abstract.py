from django.test import TestCase
from ..models import Abstract, Paper


class AbstractTest(TestCase):
    """ Test module for Abstract model """

    def setUp(self):
        Abstract.objects.create(
            title='etf abstract', description='Lorem ipsum', paper=Paper.objects.create(title='asdsa'))
        Abstract.objects.create(
            title='bond abstract', description='Lorem ipsum', paper=Paper.objects.create(title='asdsa'))

    def tearDown(self):
        pass

    def test_abstract_title(self):
        abstract_etf = Abstract.objects.get(title='etf abstract')
        abstract_bond = Abstract.objects.get(title='bond abstract')
        self.assertEqual(
            abstract_etf.get_title(), "Abstract: etf abstract")
        self.assertEqual(
            abstract_bond.get_title(), "Abstract: bond abstract")
