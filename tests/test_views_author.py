import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Author, Paper
from ..serializers import AuthorSerializer
from django.contrib.auth.models import User


# initialize the APIClient app
# user = User.objects.create(username='aisha', password='bobrules')
client = Client()
client.login(username='aisha', password='bobrules')

class GetAllAuthorTest(TestCase):
	""" Test module for GET all author API """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		Author.objects.create(first_name='Casper', last_name='Black', paper=Paper.objects.create(title='asdasd'))
		Author.objects.create(first_name='Muffin',last_name='Brown', paper=Paper.objects.create(title='asdasd'))
		Author.objects.create(first_name='Rambo', last_name='Black', paper=Paper.objects.create(title='asdasd'))
		Author.objects.create(first_name='Ricky', last_name='Brown', paper=Paper.objects.create(title='asdasd'))

	def test_get_all_authors(self):
	# get API response
		response = client.get(reverse('get_post_author'))
		# get data from db
		authors = Author.objects.all()
		serializer = AuthorSerializer(authors, many=True)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleAuthorTest(TestCase):
	""" Test module for GET single autho API """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Author.objects.create(first_name='Casper', last_name='Black', paper=Paper.objects.create(title='asdasd'))
		self.cuffin = Author.objects.create(first_name='Muffin',last_name='Brown', paper=Paper.objects.create(title='asdasd'))
		self.cambo = Author.objects.create(first_name='Rambo', last_name='Black', paper=Paper.objects.create(title='asdasd'))
		self.cicky = Author.objects.create(first_name='Ricky', last_name='Brown', paper=Paper.objects.create(title='asdasd'))

	def test_get_valid_single_author(self):
		response = client.get(reverse('get_delete_update_author', kwargs={'pk': self.cambo.pk}))
		autho = Author.objects.get(pk=self.cambo.pk)
		serializer = AuthorSerializer(autho)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_get_invalid_single_author(self):
		response = client.get(reverse('get_delete_update_author', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewAuthorTest(TestCase):
	""" Test module for inserting a new author """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.valid_payload = {
			'first_name': 'Muffin',
			'last_name': 'lorem ipsum',
			'middle_name': 'dsfsdf'
		}
		self.invalid_payload = {
			'first_name': '',
			'last_name': 'lorem ipsum'
		}

	def test_create_valid_author(self):
		response = client.post(reverse('get_post_author'),data=json.dumps(self.valid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_invalid_author(self):
		response = client.post(reverse('get_post_author'),data=json.dumps(self.invalid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleAuthorTest(TestCase):
	""" Test module for updating an existing authors record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Author.objects.create(first_name='Casper', last_name='Black', paper=Paper.objects.create(title='asdasd'))
		self.cuffin = Author.objects.create(first_name='Muffin',last_name='Brown', paper=Paper.objects.create(title='asdasd'))

		self.valid_payload = {
			'first_name': 'Buffy',
			'last_name': 'ssssBlack',
			'middle_name': 'dsfsdf'
			# 'paper': self.cuffin.paper.id
		}
		self.invalid_payload = {
			'first_name': '',
			'last_name': 'lorem'
		}

	def test_valid_update_author(self):

		response = client.put(reverse('get_delete_update_author',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.valid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_update_author(self):
		response = client.put(reverse('get_delete_update_author',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.invalid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleAuthorTest(TestCase):
	""" Test module for deleting an existing author record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Author.objects.create(
		first_name='Casper', last_name='Black', paper=Paper.objects.create(title='asdasd'))
		self.muffin = Author.objects.create(
		first_name='Muffy', last_name='Brown', paper=Paper.objects.create(title='asdasd'))

	def test_valid_delete_author(self):
		response = client.delete(
		reverse('get_delete_update_author', kwargs={'pk': self.muffin.pk}))
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_delete_author(self):
		response = client.delete(
		reverse('get_delete_update_author', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)