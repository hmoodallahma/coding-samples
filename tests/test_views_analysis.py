import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Analysis, Paper
from ..serializers import AnalysisSerializer
from django.contrib.auth.models import User


# initialize the APIClient app
# user = User.objects.create(username='aisha', password='bobrules')
client = Client()
client.login(username='aisha', password='bobrules')

class GetAllAnalysesTest(TestCase):
	""" Test module for GET all analysis API """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		Analysis.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		Analysis.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdasd'))
		Analysis.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='asdasd'))
		Analysis.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='asdasd'))

	def test_get_all_analyses(self):
	"""test get all analysis from api"""
		response = client.get(reverse('get_post_analysis'))
		# get data from db
		analyses = Analysis.objects.all()
		serializer = AnalysisSerializer(analyses, many=True)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleAnalysisTest(TestCase):
	""" Test module for GET single analysis API """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Analysis.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.cuffin = Analysis.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdasd'))
		self.cambo = Analysis.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.cicky = Analysis.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='asdasd'))

	def test_get_valid_single_analysis(self):
		response = client.get(reverse('get_delete_update_analysis', kwargs={'pk': self.cambo.pk}))
		analysis = Analysis.objects.get(pk=self.cambo.pk)
		serializer = AnalysisSerializer(analysis)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_get_invalid_single_analysis(self):
		response = client.get(reverse('get_delete_update_analysis', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewAnalysisTest(TestCase):
	""" Test module for inserting a new analysis """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.valid_payload = {
			'title': 'Muffin',
			'description': 'lorem ipsum',
			'paper': Paper.objects.create(title='sdfsdf').id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem ipsum'
		}

	def test_create_valid_analysis(self):
		"""Test that a valid analysis was created"""
		response = client.post(reverse('get_post_analysis'),data=json.dumps(self.valid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_invalid_analysis(self):
		response = client.post(reverse('get_post_analysis'),data=json.dumps(self.invalid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleAnalysisTest(TestCase):
	""" Test module for updating an existing analysis record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Analysis.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.cuffin = Analysis.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdasd'))

		self.valid_payload = {
			'title': 'Buffy',
			'description': 'ssssBlack',
			'paper': self.cuffin.paper.id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem'
		}

	def test_valid_update_analysis(self):
		response = client.put(reverse('get_delete_update_analysis',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.valid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_update_analysis(self):
		response = client.put(reverse('get_delete_update_analysis',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.invalid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleAnalysisTest(TestCase):
	""" Test module for deleting an existing analysis record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Analysis.objects.create(
		title='Casper', description='Black', paper=Paper.objects.create(title='asdasd'))
		self.muffin = Analysis.objects.create(
		title='Muffy', description='Brown', paper=Paper.objects.create(title='asdasd'))

	def test_valid_delete_analysis(self):
		response = client.delete(
		reverse('get_delete_update_analysis', kwargs={'pk': self.muffin.pk}))
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_delete_analysis(self):
		response = client.delete(
		reverse('get_delete_update_analysis', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)