import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Research, Paper
from ..serializers import ResearchSerializer
from django.contrib.auth.models import User


# user = User.objects.create(username='aisha', password='bobrules')
client = Client()
client.login(username='aisha', password='bobrules')

class GetAllResearchesTest(TestCase):
	""" Test module for GET all researches API """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		Research.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdsa'))
		Research.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdsa'))
		Research.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='asdsa'))
		Research.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='asdsa'))

	def test_get_all_researches(self):
	# get API response
		response = client.get(reverse('get_post_research'))
		# get data from db
		researches = Research.objects.all()
		serializer = ResearchSerializer(researches, many=True)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleResearchTest(TestCase):
	""" Test module for GET single research API """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper =Research.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdsa'))
		self.cuffin = Research.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdsa'))
		self.cambo = Research.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='asdsa'))
		self.cicky =Research.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='asdsa'))

	def test_get_valid_single_research(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		response = client.get(reverse('get_delete_update_research', kwargs={'pk': self.cambo.pk}))
		research = Research.objects.get(pk=self.cambo.pk)
		serializer = ResearchSerializer(research)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_get_invalid_single_research(self):
		response = client.get(reverse('get_delete_update_research', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewResearchTest(TestCase):
	""" Test module for inserting a new research"""
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.valid_payload = {
			'title': 'Muffin',
			'description': 'lorem ipsum',
			'paper': Paper.objects.create(title='sdfsdf').id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem ipsum'
		}

	def test_create_valid_research(self):
		response = client.post(reverse('get_post_research'),data=json.dumps(self.valid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_invalid_research(self):
		response = client.post(reverse('get_post_research'),data=json.dumps(self.invalid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleResearchTest(TestCase):
	""" Test module for updating an existing research record"""

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Research.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='asdsa'))
		self.cuffin = Research.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='asdsa'))

		self.valid_payload = {
			'title': 'Buffy',
			'description': 'ssssBlack',
			'paper': self.cuffin.paper.id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem'
		}

	def test_valid_update_research(self):
		response = client.put(reverse('get_delete_update_research',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.valid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_update_research(self):
		response = client.put(reverse('get_delete_update_research',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.invalid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleResearchTest(TestCase):
	""" Test module for deleting an existing research record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Research.objects.create(
		title='Casper', description='Black', paper=Paper.objects.create(title='asdsa'))
		self.muffin = Research.objects.create(
		title='Muffy', description='Brown', paper=Paper.objects.create(title='asdsa'))

	def test_valid_delete_research(self):
		response = client.delete(
		reverse('get_delete_update_research', kwargs={'pk': self.muffin.pk}))
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_delete_research(self):
		response = client.delete(
		reverse('get_delete_update_research', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)