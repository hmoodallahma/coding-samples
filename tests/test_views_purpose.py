import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Purpose, Paper
from ..serializers import PurposeSerializer
from django.contrib.auth.models import User


# user = User.objects.create(username='aisha', password='bobrules')
client = Client()
# client.login(username='aisha', password='bobrules')

class GetAllPurposesTest(TestCase):
	""" Test module for GET all purpose API """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		Purpose.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='dfsdfs'))
		Purpose.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='dfsdfs'))
		Purpose.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='dfsdfs'))
		Purpose.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='dfsdfs'))

	def test_get_all_purposes(self):
	# get API response
		response = client.get(reverse('get_post_purpose'))
		# get data from db
		purposes = Purpose.objects.all()
		serializer = PurposeSerializer(purposes, many=True)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSinglePurposeTest(TestCase):
	""" Test module for GET single purpose API """
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Purpose.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='dfsdfs'))
		self.cuffin = Purpose.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='dfsdfs'))
		self.cambo = Purpose.objects.create(title='Rambo', description='Black', paper=Paper.objects.create(title='dfsdfs'))
		self.cicky = Purpose.objects.create(title='Ricky', description='Brown', paper=Paper.objects.create(title='dfsdfs'))

	def test_get_valid_single_purpose(self):
		response = client.get(reverse('get_delete_update_purpose', kwargs={'pk': self.cambo.pk}))
		purpose = Purpose.objects.get(pk=self.cambo.pk)
		serializer = PurposeSerializer(purpose)
		self.assertEqual(response.data, serializer.data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_get_invalid_single_purpose(self):
		response = client.get(reverse('get_delete_update_purpose', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewPurposeTest(TestCase):
	""" Test module for inserting a new purpose"""
	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.valid_payload = {
			'title': 'Muffin',
			'description': 'lorem ipsum',
			'paper': Paper.objects.create(title='sdfsdf').id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem ipsum'
		}

	def test_create_valid_purpose(self):
		response = client.post(reverse('get_post_purpose'),data=json.dumps(self.valid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_invalid_purpose(self):
		response = client.post(reverse('get_post_purpose'),data=json.dumps(self.invalid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSinglePurposeTest(TestCase):
	""" Test module for updating an existing purpose record"""

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Purpose.objects.create(title='Casper', description='Black', paper=Paper.objects.create(title='dfsdfs'))
		self.cuffin = Purpose.objects.create(title='Muffin',description='Brown', paper=Paper.objects.create(title='dfsdfs'))

		self.valid_payload = {
			'title': 'Buffy',
			'description': 'ssssBlack',
			'paper': self.cuffin.paper.id
		}
		self.invalid_payload = {
			'title': '',
			'description': 'lorem'
		}

	def test_valid_update_purpose(self):
		response = client.put(reverse('get_delete_update_purpose',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.valid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_update_purpose(self):
		response = client.put(reverse('get_delete_update_purpose',
			kwargs={'pk': self.cuffin.pk}),
			data=json.dumps(self.invalid_payload),
			content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSinglePurposeTest(TestCase):
	""" Test module for deleting an existing purpose record """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.casper = Purpose.objects.create(
		title='Casper', description='Black', paper=Paper.objects.create(title='dfsdfs'))
		self.muffin = Purpose.objects.create(
		title='Muffy', description='Brown', paper=Paper.objects.create(title='dfsdfs'))

	def test_valid_delete_purpose(self):
		response = client.delete(
		reverse('get_delete_update_purpose', kwargs={'pk': self.muffin.pk}))
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

	def test_invalid_delete_purpose(self):
		response = client.delete(
		reverse('get_delete_update_purpose', kwargs={'pk': 30}))
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)