from django.test import TestCase
from ..models import Analysis, Paper


class AnalysisTest(TestCase):
    """ Test module for Analysis model """

    def setUp(self):
        paper = Paper.objects.create(title='sdfsdf')
        paper1 = Paper.objects.create(title='sdfsdf1')
        Analysis.objects.create(
            title='etf analysis', description='Lorem ipsum', paper=paper)
        Analysis.objects.create(
            title='bond analysis', description='Lorem ipsum', paper=paper1)

    def tearDown(self):
        pass

    def test_analysis_title(self):
        analysis_etf = Analysis.objects.get(title='etf analysis')
        analysis_bond = Analysis.objects.get(title='bond analysis')
        self.assertEqual(
            analysis_etf.get_title(), "Analysis: etf analysis")
        self.assertEqual(
            analysis_bond.get_title(), "Analysis: bond analysis")
