import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import *
from ..serializers import PaperSerializer
from django.contrib.auth.models import User

# user = User.objects.create(username='aisha', password='bobrules')
client = Client()


class CreateNewPaperTest(TestCase):
	""" Test module for inserting a new puppy """

	def setUp(self):
		client.force_login(User.objects.get_or_create(username='testuser')[0])
		self.valid_payload = {
			'title' : 'MUFFINSS',
			'purpose':{
				'title': 'Muffin',
				'description': 'lorem ipsum',
			},
			'hypothesis':{
				'title': 'Muffin',
				'description': 'lorem ipsum',
			},
			'research':{
				'title': 'Muffin',
				'description': 'lorem ipsum',
			},
			'method':{
				'title': 'Muffin',
				'description': 'lorem ipsum',
			},
			'analysis':{
				'title': 'Muffin',
				'description': 'lorem ipsum',
			},
			'results':{
				'title': 'Muffin',
				'description': 'lorem ipsum',
			},
			'abstract':{
				'title': 'Muffin',
				'description': 'lorem ipsum',
			}
		}
		self.invalid_payload = {
			'title': '',
			'abstract':{
				'title': 'Muffin',
				'description': 'lorem ipsum',
			},
			'abstract':{
				'title': 'Muffin',
				'description': 'lorem ipsum',
			},
		}


	def tearDown(self):
		pass



	def test_create_valid_results(self):
		response = client.post(reverse('get_post_paper'),data=json.dumps(self.valid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)

	def test_create_invalid_results(self):
		response = client.post(reverse('get_post_paper'),data=json.dumps(self.invalid_payload),content_type='application/json')
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)