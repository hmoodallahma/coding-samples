from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view,permission_classes, authentication_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication, BasicAuthentication
from rest_framework.response import Response
from dirty.models import *
from dirty.serializers import *
from dirty.etfs.output_holdings import *
from dirty.etfs.scrape_form4 import get_filings_table_with_links




@api_view(['GET'])
"""All Categories """
@permission_classes([IsAuthenticated])
def etf_cat_collection(request):
    if request.method == 'GET':
        entries = Category.objects.all()
        serializer = DirtyCategorySerializer(entries, many=True)
        return Response(serializer.data)

@api_view(['GET'])
"""Ouput ETF and All holdings"""
@permission_classes([IsAuthenticated])
def etf_collection(request):
    if request.method == 'GET':
        entries = ETF.objects.all()
        serializer = DirtyEtfSerializer(entries, many=True)
        return Response(serializer.data)


"""All Stocks"""
@permission_classes([IsAuthenticated])
def stock_collection(request):
    if request.method == 'GET':
        entries = Stock.objects.all()
        serializer = StockSerializer(entries, many=True)
        return Response(serializer.data)

"""All Symbols"""
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def symbol_collection(request):
    if request.method == 'GET':
        entries = Symbol.objects.all()
        serializer = list(map(lambda x: serialize_symbol(x), entries))
        print(serializer)
        return Response(serializer)

 
@api_view(['GET'])
"""Output queried ETF"""
@permission_classes([IsAuthenticated])
def etf_holdings(request, name):
    if request.method == 'GET':
        entries = ETF.objects.get(name=name)
        serializer = DirtyEtfSerializer(entries)
        return Response(serializer.data)


@api_view(['GET'])
"""Holdings for queried ETF"""
@permission_classes([IsAuthenticated])
def etf_dataframe(request, symbol):
    if request.method == 'GET':
        etf = ETF.objects.get(symbol=symbol)
        df = make_holdings_df(etf)
        return Response(df.to_html())

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def etf_detail(request, symbol):
    if request.method == 'GET':
        etf = ETF.objects.get(symbol=symbol)
        df = make_holdings_df(etf)
        return Response({'name': etf.name,'table':df.to_html()})

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def test_etfs(request):
    if request.method == 'GET':
        entries = ETF.objects.all()
        serializer = DirtyEtfSerializer(entries, many=True)
        return Response(serializer.data)


@permission_classes([IsAuthenticated])
def test_etfs(request):
    if request.method == 'GET':
        entries = ETF.objects.all()
        serializer = DirtyEtfSerializer(entries, many=True)
        return Response(serializer.data)



@permission_classes([IsAuthenticated])
def get_form4s(request, url):
    if request.method == 'GET':
        df, meta = get_filings_table_with_links(url)
        return Response({'meta': meta,'table':df.to_html()})