from rest_framework import serializers
from dirty.models import *
import ast

class DirtyEftCategorySerializer(serializers.ModelSerializer):
	# etf = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
	# owner = PrimaryKeyRelatedField(queryset=ETF.objects.all())
	etf = serializers.StringRelatedField(many=False)
	category = serializers.StringRelatedField(many=False)

	class Meta:
		model = ETF_Category
		fields = ('etf', 'category')


class StockSerializer(serializers.ModelSerializer):
	class Meta:
		model = Stock
		fields = ['symbol']

def serialize_symbol(symbol):
    return {
        'id': symbol.id,
        'name': symbol.name,
        'meta': ast.literal_eval(symbol.meta)
    }

class ExchangeSerializer(serializers.ModelSerializer):
	class Meta:
		model = Exchange
		fields = ['name','symbol']

class SecuritySerializer(serializers.ModelSerializer):
   def to_representation(self, value):
        """
        Serialize tagged objects to a simple textual representation.
        """
        if isinstance(value, Stock):
            return value.symbol
        elif isinstance(value, NonStock):
            return value.name
        raise Exception('Unexpected type of tagged object')

class HoldingSerializer(serializers.ModelSerializer):
	security = SecuritySerializer()
	allocation = serializers.FloatField()
	class Meta:
		model = Holding
		fields= ['security', 'allocation']
		# extra_kwargs = {
  #           'allocation': {'max_digits': 5, 'decimal_places': 2}
  #       }

class DirtyEtfSerializer(serializers.ModelSerializer):
	holdings = HoldingSerializer(many=True)
	categories = serializers.StringRelatedField(many=True)
	class Meta:
		model = ETF
		fields = ('id','name', 'categories', 'holdings')


class DirtyCategorySerializer(serializers.ModelSerializer):
	etfs = serializers.StringRelatedField(many=True)
	class Meta:
		model = Category
		fields = ('name', 'etfs')