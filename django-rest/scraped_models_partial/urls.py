from django.conf.urls import url
from django.urls import path
from . import views


urlpatterns = [
	path('api/dirty/etf_cat/', views.etf_cat_collection, name='etf_cat_collection'),
	path('api/dirty/etfs/', views.etf_collection, name='etf_collection'),
	path('api/dirty/etfs/<str:name>', views.etf_holdings, name='etf_holdings'),
	path('api/dirty/etfs/dataframe/<str:symbol>', views.etf_dataframe, name='etfdataframe'),
	path('api/etfs/detail/<str:symbol>', views.etf_detail, name='etf_detail'),
	path('api/symbols/', views.symbol_collection, name='all_symbols'),
	path('api/dirty/stocks/', views.etf_collection, name='stocks_collection'),
	path('api/test/', views.test_etfs, name='test_etfs'),
	
	
]