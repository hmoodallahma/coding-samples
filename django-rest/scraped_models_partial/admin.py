from django.contrib import admin

# Register your models here.
from .models import *

class StockAdmin(admin.ModelAdmin):
    model = Stock
    extra = 0
    ordering = ('symbol','name')
    search_fields = ('name', 'symbol', )

class ETFAdmin(admin.ModelAdmin):
    model = ETF
    extra = 0
    ordering = ('id','symbol','name')
    search_fields = ('name', 'symbol', )

class StockAliasAdmin(admin.ModelAdmin):
    model = StockAlias
    extra = 0
    search_fields = ('name',)

class SymbolAliasAdmin(admin.ModelAdmin):
    model = SymbolAlias
    extra = 0
    search_fields = ('name',)

class SymbolAdmin(admin.ModelAdmin):
    model = Symbol
    extra = 0
    search_fields = ('name','symbol')

class SymbolHoldingAdmin(admin.ModelAdmin):
    model = SymbolHolding
    extra = 0
    search_fields = ('name','symbol')

class NonStockAdmin(admin.ModelAdmin):
    model = NonStock
    extra = 0
    search_fields = ('name',)

admin.site.register(ETF, ETFAdmin)
admin.site.register(Category)
admin.site.register(ETF_Category)
admin.site.register(Holding)
admin.site.register(Stock, StockAdmin)
admin.site.register(NonStock, NonStockAdmin)
admin.site.register(StockAlias, StockAliasAdmin)

admin.site.register(Symbol, SymbolAdmin)
admin.site.register(SymbolHolding, SymbolHoldingAdmin)
admin.site.register(SymbolAlias, SymbolAliasAdmin)
admin.site.register(HoldingsLabel)
admin.site.register(LabelCategory)