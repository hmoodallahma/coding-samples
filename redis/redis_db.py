from project import cx_dict, redis_db as conn
import json
import sys, redis , gc

ids_lua = conn.register_script('''
local ids = redis.call('ZRANGEBYSCORE', KEYS[1], ARGV[1], ARGV[2])
return ids
''')

df_lua = conn.register_script("""
local ids = redis.call('ZRANGEBYSCORE', KEYS[1], ARGV[1], ARGV[2]);
local value = redis.call('HMGET', KEYS[2], unpack(ids));
return value
""")

hash_lua = conn.register_script("""
local value = redis.call('HGETALL', KEYS[1])
return value
""")

ob_lua = conn.register_script("""
local keys = (redis.call('keys', ARGV[1]))
local r = {}
for _, v in pairs(keys) do
  r[#r+1] = {v, redis.call('HGETALL', v)}
end

return r
""")



def record_trade(exchange, market, event):
    # async_conn = await aioredis.create_redis_pool('redis://localhost:6379', db=0)
    id = conn.incr(market+exchange+':id')
    
    event['id'] = id
    event_key = f'{market}-{exchange}:trades'
    ref = {id: event['time']}
    pipe = conn.pipeline(True)
    #put event in hash set keyed by id
    pipe.hset(event_key,id, json.dumps(event))
    #timestamp has to be int
    #put event id in sorted set keyed by timestamp
    pipe.zadd(f'{event_key}-series', ref)
    p = pipe.execute()

def get_range_of_trades(exchange, market, sts, ets):

    try:
        market_key = f'{market}-{exchange}:trades'
        # print(market_key)
        # pipe = conn.pipeline(True)
        ids = ids_lua(keys=[f'{market_key}-series'], args=[sts,ets])
        # print(ids)
        # ids = conn.zrangebyscore(market_key+ '-series', sts, ets)
        # r = df_lua(keys=[market_key+'-series', market_key],args=[sts,ets])
        r = conn.hmget(market_key, ids)
        # pipe.execute()
        del ids , market_key
        gc.collect()
        return r
    except redis.exceptions.ResponseError:
        return False
    except:
        print(sys.exc_info()[0],"occured.")
        print(sys.exc_info()[1])
        print(sys.exc_info()[2])
        print(f"GET RANGE OF TRADES {exchange} {market}")
        del ids , market_key
        gc.collect()


def get_range_key_trades(key, sts, ets):

    try:
        market_key = f'{market}-{exchange}:trades'
        # print(market_key)
        # pipe = conn.pipeline(True)
        ids = ids_lua(keys=[f'{market_key}-series'], args=[sts,ets])
        # print(ids)
        # ids = conn.zrangebyscore(market_key+ '-series', sts, ets)
        # r = df_lua(keys=[market_key+'-series', market_key],args=[sts,ets])
        r = conn.hmget(market_key, ids)
        # pipe.execute()
        del ids , market_key
        gc.collect()
        return r
    except redis.exceptions.ResponseError:
        return False
    except:
        print(sys.exc_info()[0],"occured.")
        print(sys.exc_info()[1])
        print(sys.exc_info()[2])
        print(f"GET RANGE OF TRADES {exchange} {market}")
        del ids , market_key
        gc.collect()

def del_range_of_trades(exchange, market, sts, ets):
    market_key = f'{market}-{exchange}:trades'
    market_series = f'{market_key}-series'
    pipe = conn.pipeline(True)
    ids = ids_lua(keys=[market_series], args=[sts,ets])
    pipe.zremrangebyscore(market_series,sts,ets)
    ids.append(0)
    pipe.hdel(market_key, *ids)
    # pipe.hdel(market_key, ids)
    x = pipe.execute()

    del ids
    gc.collect()
    # return hash_lua(keys=[market_key])
    # to db
    #removed written keys from hash
    #remove written ids from sorted set


def get_ob(market):
    return ob_lua(args=[f'{market}_*_*'])



# def connection_event_id(key,start, end):
#     counts = count_types_lua(keys=[key], args=[start, end])
#     return json.loads(counts)

# def get_df(key1,key2,start,end):
#     counts = df_lua(keys=[key1,key2], args=[start, end])
#     # print(counts)
#     return json.loads(counts)

def count_type(type, start, end):
    type_key = 'events:{type}'.format(type=type)
    return conn.zcount(type_key, start, end)



def record_event_by_type(event):
    id = conn.incr('event:id')
    event['id'] = id
    event_key = 'event:{id}'.format(id=id)
    type_key = 'events:{type}'.format(type=event['type'])

    ref = {id: event['timestamp']}
    pipe = conn.pipeline(True)
    pipe.hmset(event_key, event)
    pipe.zadd('levents', ref)
    pipe.zadd(type_key, ref)
    pipe.execute()

def record_event_types(event):
    id = conn.incr('event:id')
    event['id'] = id
    event_key = 'event:{id}'.format(id=id)
    type_key = 'events:{type}'.format(type=event['type'])

    ref = {id: event['timestamp']}
    pipe = conn.pipeline(True)
    pipe.hmset(event_key, event)
    pipe.zadd('events', **ref)
    pipe.zadd(type_key, **ref)
    pipe.sadd('event:types', event['type'])
    pipe.execute()

def count_types_fast( start, end):
    event_types = conn.smembers('event:types')
    counts = {}
    for event_type in event_types:
        counts[event_type] = conn.zcount(
            'events:{type}'.format(type=event_type), start, end)
    return counts