from models.player import Player, PlayerMatch
from models.match import Match, MatchOutcome
from models.pool import Pool
from models.pool_result import PoolResult
import unittest


"""test players are created with name and id
	and matchplayers inherit attributes""" 
class TestPlayer(unittest.TestCase):
	"""make sure the data types are returned as expected"""
	def test_data_types(self):
		player = Player('aisha',1)

		self.assertEqual(type(player.name), str)
		self.assertEqual(type(player.id), int)

	def test_player_match_attributes_inherited(self):

		opponent = Player('patric',2)

		player_match = PlayerMatch('aisha',1, opponent.id)

		self.assertEqual(player_match.name, 'aisha')
		self.assertEqual(player_match.id, 1)
		self.assertEqual(player_match.opponent_id, 2)


"""test matches are played to 5 points
	and a winner and loser is return in result"""
class TestMatch(unittest.TestCase):

	"""create two players to test match play"""
	def setUp(self):
		self.player1 = Player('aisha',1)
		self.player2 = Player('patric',2)

	def test_match_created(self):
		"""test that the players are entered correctly in the match"""
		match = Match(self.player1, self.player2)

		self.assertEqual(match.player1.name, 'aisha')
		self.assertEqual(match.player1.opponent_id, 2)
		self.assertEqual(match.player2.name, 'patric')
		self.assertEqual(match.player2.opponent_id, 1)
		self.assertEqual(len(match.players),2)

	def test_hit_increments_score(self):
		"""test that the when a hit is scored
			the match score gets updated"""
		match = Match(self.player1, self.player2)
		match.hit()
		self.assertNotEqual(match.scores, [0,0])


	def test_match_played(self):
		"""test match played to 5 points"""
		"""test there is a winner and a loser"""
		match = Match(self.player1, self.player2)
		match.aller()
	
		self.assertEqual(max(match.scores), 5)
		self.assertFalse(match.winner == None)
		self.assertFalse(match.loser == None)
		self.assertFalse(match.winner == match.loser)
		self.assertTrue(match.winner.match_score == 5)
		self.assertTrue(match.loser.match_score != 5)

	def test_match_results_data_types(self):
		match = Match(self.player1, self.player2)
		match.aller()
		results = match.return_match_result()

		self.assertEqual(type(results.winner_id), int)
		self.assertEqual(type(results.loser_id), int)
		self.assertEqual(type(results.loser_score), int)
		self.assertEqual(type(results.winner_score), int)

from collections import defaultdict
"""Test to make sure pools are set up as expected"""
class TestPool(unittest.TestCase):

	def setUp(self):
		player_names = ['patrick', 'aleks', 'john', 'adrian', 'andrew', 'aisha', 'bob']
		self.players = [Player(n, i+1) for i, n in enumerate(player_names)] 

	def test_all_players_play(self):
		"""test all players play eachother 
		   and do not play themselves"""
		pool = Pool(self.players)
		match_dict = defaultdict(list)
		
		mm = pool.create_matches()
		for m in pool.matches:
			match_dict[m[0]].append(m[1])
			match_dict[m[1]].append(m[0])

		for item in match_dict:
			self.assertEqual(len(match_dict[item]), len(self.players)-1)
			self.assertNotIn(item, match_dict[item])
		
"""Test pools are played and results are output"""

class TestPoolResult(unittest.TestCase):
	
	def setUp(self):
		player_names = ['patrick', 'aleks', 'john', 'adrian', 'andrew', 'aisha', 'bob']
		players = [Player(n, i+1) for i, n in enumerate(player_names)] 
		self.pool = Pool(players)
		self.pr = PoolResult(self.pool)

	
	def test_add_together_hits_for(self):
		"""get the result matrix for scores to calculate hits for"""
		
		results = self.pr.create_result_matrix(False)
		hits_for = sum(results[0])
		self.assertEqual(type(hits_for), int)
		self.assertNotEqual(0, hits_for)

	def test_add_together_against_for(self):
		"""get the result matrix for scores to calculate hits against"""
		
		results = self.pr.create_result_matrix(False)
		hits_against = 0
		index = 0
		for row in results:
			hits_against += row[index]
		
		self.assertEqual(type(hits_against), int)
		self.assertNotEqual(0, hits_against)

	def test_rank_by_net_hits(self):
		"""make sure each player is assigned a rank by hits"""
		"""make sure the lowest net hits gets lowest rank"""
		"""highest rank is 1"""
		result = self.pr.rank_by_net_hits()
		
		rank_map = list(map(lambda key: result[key]['net_hits_rank'], result.keys()))
		lowest_net = min(list(map(lambda key: result[key]['net_hits'], result.keys())))
		"""todo: test that the key with the lowest rank has the lowest score"""

		self.assertEqual(type(result[1]['net_hits']), int)
		self.assertEqual(rank_map, list(range(1, len(result) + 1)))
	
	def test_rank_by_wins(self):
		"""make sure each player is assigned a rank by wins"""
		"""todo: make sure the highest wins gets highest rank"""
		results = self.pr.create_result_matrix(False)
		self.pr.rank_by_wins()
		rank_map = list(map(lambda key: result[key]['win_rank'], result.keys()))
		lowest_net = min(list(map(lambda key: result[key]['wins'], result.keys())))
		print(self.pr.standings)

		self.assertEqual(type(result[1]['net_hits']), int)
		self.assertEqual(rank_map, list(range(1, len(result) + 1)))
		

if __name__ == '__main__':
    unittest.main()