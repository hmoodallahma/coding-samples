from .player import PlayerMatch
import random

class Match:
    def __init__(self, player1, player2):
        self.player1 = PlayerMatch(player1.name, player1.id, player2.id)
        self.player2 = PlayerMatch(player2.name, player2.id, player1.id)
        self.winner, self.loser = None, None
        self.scores = [0,0]
        self.players = [self.player1, self.player2]
        

    def __repr__(self):
        """say who won first"""
        return '{} vs {}: outcome {}-{}'.format(self.winner, self.loser,
                                                self.winner.match_score,
                                                self.loser.match_score)
    def hit(self):
        """randomly assign hit"""
        hitter= random.choice(self.players)
        hitter.touche()
        self.update_scores()
        return None
    
    def update_scores(self):
        self.scores = [player.match_score for player in self.players]

    def determine_winner(self):
        """ the player with the highest score is the winner"""
        self.winner = self.players[self.scores.index(max(self.scores))]
        return None
    
    def aller(self):
        """matches go up to 5 points"""
        while (max(self.scores) < 5):
            self.hit()   
        self.determine_winner()
        
        for player in self.players:
            player.update_hits(*self.scores)
            self.scores.reverse()
            if player != self.winner:
                self.loser = player
        return self.return_match_result()
    
    def return_match_result(self):
        
        return MatchOutcome(self.winner.id, self.loser.id,
                            self.winner.match_score,
                            self.loser.match_score)
    
from collections import namedtuple
"""simulating database entries"""
MatchOutcome = namedtuple('MatchOutcome',
     ['winner_id', 'loser_id', 'winner_score', 'loser_score'])