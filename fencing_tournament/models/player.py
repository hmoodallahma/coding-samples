#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 10:30:08 2021

@author: aisha
"""

"""Create a fencing pool give a number of players and the outcomes of matches"""
"""assume double hits do not count and ties are allowed"""
import random

class Player:
    def __init__(self, name, player_id=0):
        self.name = name
        self.id = player_id
         
    def set_id(self, player_id):
        self.id = player_id
        return None
    
    def __repr__(self):
        return str(self.__dict__)
   

    

"""Todo: abstract match details for player to PlayerMatch"""
class PlayerMatch(Player):
    def __init__(self, name, player_id, opponent_id, hits_for=0, hits_against=0):

        super().__init__(name, player_id)
        self.opponent_id = opponent_id
        self.hits_for = hits_for
        self.hits_against = hits_against
        self.match_score = 0
    
    def __repr__(self):
        return self.name
    
    def update_hits(self, hits_for, hits_against):

        self.hits_for= self.hits_for + hits_for
        self.hits_against =  self.hits_against +  hits_against
        self.net_hits = self.hits_for - self.hits_against
        return None     
    
    def touche(self, score=1):
        self.match_score += score
        return None
