import random
from collections import defaultdict
from .match import Match

class PoolResult:
    def __init__(self, pool):
        self.pool = pool
        self.results = []
        self.get_results()
        self.rank = []
        self.standings = defaultdict(defaultdict)

    def get_results(self):
       """mocking play"""
       for m in self.pool.matches:
            p1, p2 = [self.pool.players[k-1] for k in m]
            played_match = Match(p1, p2)
            """to do: add a way to input hits"""
            
            match_result = played_match.aller()
            self.results.append(match_result)
    
    def count_wins(self, games):
        return len(list(filter(lambda x: x >= 5, games)))

    def rank_by_wins(self):
        """most wins gets highest rank"""
        results = self.create_result_matrix(False)
        win_dict = defaultdict(defaultdict)
        for i, result in enumerate(results):
            print(result)
            win_dict[i+1] = self.count_wins(result)
        
        ranks = sorted(win_dict.items(), key=lambda item: item[1], reverse=True)
        
        for i, r in enumerate(ranks):
            self.standings[r[0]]['win_rank'] = i + 1
            self.standings[r[0]]['wins'] = r[1]

        return None

    def return_top_three(self, key):
        return min(self.standings.items(), key=lambda x: x[key])
    
    def rank_by_net_hits(self):
        """calculate rank by net hits in case of ties"""
        """highest net hits gets highest rank"""
        results = self.create_result_matrix(False)
        result_dict = self.calc_hits_for_against(results)
        ranks = sorted(result_dict.items(), key=lambda item: item[1], reverse=True)
        for index, r in enumerate(ranks):
            self.standings[r[0]]['net_hits_rank'] = index + 1
            self.standings[r[0]]['net_hits'] = r[1]

        return self.standings
        

    def calc_hits_for_against(self, results):
        result_dict = defaultdict(defaultdict)
        net_hits = defaultdict()

        for index, row in enumerate(results):
            """tally hits for"""
            result_dict[index+1]['hits_for'] = sum(row)
            for item, value in enumerate(row):
                """append hits against to list"""
                result_dict[item+1].setdefault('hits_against', []).append(value)

        """sum hits against for all players"""
        for k in result_dict:
            result_dict[k]['hits_against'] = sum(result_dict[k]['hits_against'])
            net_hits[k] = result_dict[k]['hits_for'] - result_dict[k]['hits_against']

        return dict(net_hits)


    def create_result_matrix(self, print_pretty=True):
        matches = self.pool.create_match_matrix(print_pretty)
        
        for r in self.results:
            matches[r.winner_id-1][r.loser_id-1] = r.winner_score
            matches[r.loser_id-1][r.winner_id-1] = r.loser_score
        return matches

    def print_result_matrix(self):
        matches = self.create_result_matrix()
        top_row, table = '  ',''
        for e, m in enumerate(matches):
            top_row += ' {} '.format(e + 1)
            t = ''
            for i in m:
                t += ' {} '.format(i)
            table += '{} {}\n'.format(e+1, t)
        print(top_row)
        print(table)
        """example output
               1  2  3  4  5  6  7 
            1  X  3  1  5  3  5  5 
            2  5  X  2  5  5  5  2 
            3  1  5  X  5  1  3  2 
            4  4  1  3  X  3  1  1 
            5  5  3  1  5  X  1  1 
            6  3  3  5  1  1  X  4 
            7  2  5  5  1  5  5  X  """
        """todo: add rank to table and calculate net hits"""
        """output pretty version of table with names and results"""
    
    def create_rank_matrix(self):
        results = self.create_rank_matrix()
        rank_matrix = defaultdict()
        for row in results:
            rank_matrix[i]['hits_for'] = reduce()
"""to do:create ranked pool results to simulated database entries"""