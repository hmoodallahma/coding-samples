import random 
class Pool:
    """players will face eachother once time in each pool"""
    def __init__(self, players):
        self.players = players
        self.matches = []
        self.played_matches = []

        self.setUp()
    
    def setUp(self):
        self.design_pool()
        self.order_matches(self.create_matches())


    def possible_matches(self):
        """players play eachother only once"""
        n = len(self.players)   
        return n * (n-1) // 2
    
    def randomize_order(self):
        temp_dict = self.players.copy()
        self.players = []
        while len(temp_dict) > 0:
            mv_player = random.choice(temp_dict)
            self.players.append(mv_player)
            temp_dict.remove(mv_player)
        
    def design_pool(self):
        """will change the id of the player for the matches in the pool"""
        self.randomize_order()
        [p.set_id(i+1) for i,p in enumerate(self.players)]
        self.matches = self.create_matches()
        
        return None
    
    def create_match_matrix(self, print_pretty=True):
         l = len(self.players)
         adj_char = 'X'
         if not print_pretty:
            adj_char = 0

         m = [[adj_char] + [1] * (l-1)]
         for i in range(1,l):
            m.append([])
            for j in range(l):
                if i == j and m[i-1][j] == 0:
                    m[i].append(1)
                elif i==j:
                     m[i].append(adj_char)
                else:
                    m[i].append(0)
         return m

    def create_matches(self):
        matches = []
        for i in range(1, len(self.players)+1):
            for j in range(i+1, len(self.players)+1):
                matches.append([i,j])
        return matches
    

    def check_last_match(self,last_match, new_match):
        """check if players played in the previous match"""
        return len(set(last_match + new_match)) == len(last_match + new_match)
        
    
    
    def order_matches(self, matches):
        """order the matches so players can avoid playing back-to-back"""
        self.matches, temp_matches = matches[:1], matches[1:]
        total_matches = self.possible_matches()
        old = self.matches[0]
        while len(self.matches) < total_matches:
            for m in temp_matches:  
                no_repeats = self.check_last_match(old, m)
                tries = 0
                while not no_repeats and tries < 10:
                    m = random.choice(temp_matches)
                    no_repeats = self.check_last_match(old, m)
                    tries += 1
                
                self.matches.append(m)
                temp_matches.remove(m)
                old = m
        #Todo: output as a table
        return None
#Todo: break a list of players into multiple pools of similar size
# using recursion