#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:51:25 2021

@author: aisha
"""

class HamiltonianPath:
    
    def __init__(self, adjacency_matrix):
        self.n = len(adjacency_matrix)
        self.adjacency_matrix = adjacency_matrix
        #append the first vertex in the graph to the path
        self.path = [0]
    
    def hamiltonian_path(self):
        
        if self.solve(1):
            self.solve_hamiltonian_path()
        else:
            print('There is no solution to the problem')
    
    def solve(self, position):
        if position == self.n:
            return True
    
        for vertex_index in range(1, self.n):
            if self.is_feasible(vertex_index, position):
                #include vertex with with vertex index in solution
                self.path.append(vertex_index)
                #try to include next vertex in solution
                if self.solve(position+1):
                    return True
                else:
                    #we have to backtrack
                    #we have to remove the last item from path
                    print('removing {}'.format(self.path[-1]))
                    self.path.pop()
                   
        #we have now considered all vertices without success
        print('false for {}'.format(position))
        return False
    
    def is_feasible(self, vertex, actual_position):
        #need to check whether two vertices are connected to eachother
        #two vertices are connected if they are in adjacency_matrix
        print('checking {} and {}'.format(vertex, actual_position))
        #indices start at 0
        #check if there connection between the last node added and the 
        #current node
        print(self.path)
        if self.adjacency_matrix[self.path[actual_position-1]][vertex] == 0:
            print('no connection between {} and {}'.format(vertex, actual_position))
            #no connection
            return False
        #make sure it was only visited once
        for i in range(actual_position):
            if self.path[i] == vertex:
                print('{} is in path'.format(i))
                return False
        print('feasible: {} and {}'.format(vertex, actual_position))
        return True
                
                
    def solve_hamiltonian_path(self):
        for v in self.path:
            print(v)
        
        
if __name__ == '__main__':
    m = [[0,1,0,0,0,1],
        [1,0,1,0,0,0],
        [0,1,0,0,1,0],
        [0,0,0,0,1,1],
        [0,0,1,1,0,1],
        [1,0,0,1,1,0]]
    # m = [[0,1,1,1,1],
    #     [1,0,1,1,1],
    #     [1,1,0,1,1],
    #     [1,1,1,0,1],
    #     [1,1,1,1,0]]
    hp = HamiltonianPath(m)
    hp.hamiltonian_path()
        
        