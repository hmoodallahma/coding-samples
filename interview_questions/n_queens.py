#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 14:29:09 2021

@author: aisha
"""

"""Place N Queens on a board of N x N size such that they cannot attack 
   eachother horizontally, vertically or diagonally"""

class Queens:
    
    def __init__(self, n):
        self.n = n
        self.chess_table = [[0 for i in range(n)] for j in range(n)]
    
    def solve_n_queens(self):
        #start with the first queen with index 0
        if self.solve(0):
            self.print_queens()
        
        else:
            #when considered all possible solutions with not success
            print('there is no solution to the problem.')
        
    #column index is the same as the index of the queen
    def solve(self, col_index):
        #we check if we solved the problem
        #if we placed n queens, the problem is solved
        if col_index == self.n:
            return True
        
        #try to find position for queen at col_index within a given column
        for row_index in range(self.n):
            if self.is_place_valid(row_index, col_index):
                self.chess_table[row_index][col_index] = 1
                #1 means there is a queen at the given location
                
                #call the function recursively with col_index + 1
                #try to find the location of the next queen in next column
                if self.solve(col_index + 1):
                    return True
                else:
                    #backtrack by resetting the position
                    self.chess_table[row_index][col_index] = 0
        return False
                    
    
    def is_place_valid(self, row_index, col_index):
        # check the rows - queens can attack horizontally
        # check there is one queen in the given row
        #there will be no queens in the same column
        for i in range(self.n):
            if self.chess_table[row_index][i] == 1:
                return False
        
        #check diagonals from top left to bottom right
        j = col_index
        #iterate in reversed order
        #there are no queens on the right
        for i in range(row_index, -1, -1):
            if i < 0:
                break
            if self.chess_table[i][j] == 1:
                return False
            j = j - 1
        
        #check diagonals from top right to bottom left
        j = col_index
        for i in range(row_index, self.n) :
            if j < 0:
                break
            if self.chess_table[i][j] == 1:
                return False
            j = j - 1
        
        return True
        
    
    def print_queens(self):
        for i in range(self.n):
            for j in range(self.n):
                if self.chess_table[i][j] == 1:
                    print(' Q ', end='')
                else:
                    print(' - ', end='')
            print('\n')

if __name__ == '__main__':
    queens = Queens(3)
    queens.solve_n_queens()

        