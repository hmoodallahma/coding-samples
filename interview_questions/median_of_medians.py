#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 12:37:06 2021

@author: aisha
"""

"""Median of medians algorithm"""


x = [1, -5, 0, 10, 12, 20, 3, -1]

"""kth number"""
def median_algorithm(nums, k):
    
    chunks = [nums[i:i+5] for i in range(0, len(nums), 5)]
    medians = [sorted(chunk)[len(chunks)//2] for chunk in chunks]  
    pivot_value = sorted(medians)[len(medians)//2]
    
    """partition values"""
    left_array = [n for n in nums if n < pivot_value]
    right_array = [m for m in nums if m > pivot_value]
    
    """selection phase"""
    pivot_index = len(left_array)
    
    if k < pivot_index:
        """use left array because we are looking for smaller items"""
        return median_algorithm(left_array, k)
    elif k > pivot_index:
        return median_algorithm(right_array, k-len(left_array)-1)
    else:
        return pivot_value

def select(nums, k):
    return median_algorithm(nums, k-1)