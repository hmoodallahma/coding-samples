#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 14:41:47 2021

@author: aisha
"""

"""A class to solve all possible letter combinations for a very tricky cryptogram"""

import random
from math import factorial



puzzle = 'AIL IFW NQQ R USEBGL FN HZE EOIQCKA UITQSLY'


class CryptogramSolver(object):
    vowels = list('AEIOU')
    def __init__(self, puzzle):
        self.puzzle = puzzle
        self.words = puzzle.split()
        self.letters = sorted(list(set(puzzle.replace(' ', ''))))
        self.decoded_words = []
        self.solution = ''
        self.tried_cyphers = []
        self.successful_cyphers = []
        
    
    def make_alphabet(self):
        return list('abcdefghijklmnopqrstuvwxyz'.upper())
    
    def set_solution(self, solution):
        self.solution = solution
        self.decoded_words = solution.split(' ')
        self.decoded_words.remove('')
    
    def check_one_letter_word_I_or_A(self):
        t = sorted(self.decoded_words, key=len)[0]
        if (len(t) == 1):
            result = True if (t == 'A') or (t == 'I') else False
           # print('Passed I A' if result else 'Failed I A TEST')
            return result
        return True
    
    def check_words_contains_vowels(self):
        """make sure each word contains at least one vowel"""
        for word in self.decoded_words:
            x = any(vowel in word for vowel in self.vowels)
            if x == False:
                return x
        return True
    
    def check_U_follows_Q(self):
        """make sure any Q is followed by U"""
        if 'Q' in self.solution:
            if 'QU' in self.solution:
                return True
            else:
                #print('failed QU test')
                return False
        else:
            return True
                
    def make_cypher(self):
        """create a dictionary cypher"""
        cypher = {}
        alphab = self.make_alphabet()
        for letter in self.letters:
            """Make sure letter is not the same as the cypher letter"""
            c = random.choice(alphab)
           # while c == letter:
           #     c = random.choice(alphab)
            cypher[letter] = random.choice(alphab)
            """Can delete letter from alphab because not iterating over it"""
            alphab.remove(cypher[letter])
        while self.check_tried_cyphers(cypher):
            return cypher
        self.make_cypher()
        
    
    def calculate_possible_cyphers(self):
        """determine the number of total possible cyphers"""
        x = factorial(len(self.letters)) * factorial(26)
        print('there are {} possible cyphers'.format(x))
        """To do: calculate after cypher validation rules"""
    
    def decode_word(self, cypher): 
        """apply the cypher to the puzzle"""    
        new_string = ''
        
        for word in self.words:
            for letter in word:
                new_string += cypher[letter]
            new_string  += ' '
        self.set_solution(new_string)
        return self.review_cypher()
      
    
    def print_decoded(self):
        print(' '.join(self.decoded_words))
        
    
    def review_cypher(self):
        """apply all cypher validation methods"""
        if  self.check_one_letter_word_I_or_A() \
            and self.check_words_contains_vowels() \
            and self.check_U_follows_Q():
           
            print(self.solution)
            return True
        return False
    
    def check_tried_cyphers(self, cypher):
        """check if the newly generated cypher is in the tried cyphers"""
        """if not add it to the list"""
        if cypher not in self.tried_cyphers:
            self.tried_cyphers.append(cypher)
            return True
        else:
            print('tried that already')
            return False
        
        
    def try_cyphers(self):
        """keep trying different cyphers until cypher is validated"""
        checked_words = False
        
        while not checked_words:
            cypher = self.make_cypher()
            checked_words = self.decode_word(cypher)
        


        
        
pp = CryptogramSolver(puzzle)

while 'OFF' not in pp.solution:
    pp.try_cyphers()

"""To do: abstract cypher to its own class with its validation rules"""
"""accept the dictionary as an argument"""