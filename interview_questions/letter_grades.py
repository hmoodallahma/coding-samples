#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 11:13:39 2021

@author: aisha
"""

"""Assign a letter grade to a numerical score using binary search"""

import bisect

def grade(score, breakpoints=[50,60,70,80], grades='FCDBA'):
    i = bisect.bisect(breakpoints, score)
    return grades[i]

[grade(score) for score in [41,57,87,99,49,81,75,6,77,67,61,100]]