#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 14:10:45 2021

@author: aisha
"""
"""Functions to return Fibonnacci number at index n"""
"""0 1 2 3 5 8 13 21 34  55 89..."""
"""0 1 2 3 4 5 6  7  8   9  10..."""

def fibonacci_tail(n, a=0, b=1):
    """fibonnacci number by tail recusion"""
    if n == 0:
        return a
    if n == 1:
        return b
    print('{} {} {}'.format(n, a, b))
    return fibonacci_tail(n-1, b, a + b)

def fibonacci_itter(n):
	"""fibonnacci number by iteration"""
	a, b = 0, 1
	for i in range(0, n):
		a,b = b, a+b
	return a