#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 11:26:43 2021

@author: aisha
"""

"""Solve maze of N x N size"""

class Maze:
    def __init__(self, maze_table):
        self.maze_table = maze_table
        self.n = len(maze_table)
        self.solution = [[0 for i in range(self.n)] for j in range(self.n)]
        
    
    def solve_maze(self):
        """0 represent walls, 1 represent path"""
        if self.solve(1, 0, 0):
            self.print_solution()
        else:
            print('There is no solution to the maze!')
            return False        
    
    def is_finished(self,x,y):
        if x == self.n-1 and y == self.n-1:
            return True
    
    def solve(self,counter, x, y):
        
        if self.is_finished(x,y):
            return True
        
        if self.is_valid_move(x, y):
            self.solution[x][y] = 'S'
            counter += 1
            #advance in the x direction
            if self.solve(counter,x+1,y):
                return True
            
            #advance in the y direction
            if self.solve(counter,x,y+1):
                return True
        
            # #retreat in the x direction
            # if self.solve(counter,x-1,y):
            #     return True
            
            # #retreat in the y direction
            # if self.solve(counter,x,y-1):
            #     return True

        
            #backtrack
            self.solution[x][y] = 0
        return False

    def is_valid_move(self, x, y):
        if x < 0 or x >= self.n: return False
        if y < 0 or y >= self.n: return False
        if self.maze_table[x][y] == 0: return False
        return True
    
    def print_solution(self):
        for x in self.solution:
            for y in x:
                print(y, end='')
            print('\n')
    
if __name__ == '__main__':
    
    maze_table = [[1,1,1,1,1],
                  [1,0,0,1,0],
                  [0,0,0,1,1],
                  [1,1,1,1,0],
                  [1,1,1,1,1]]
    m = Maze(maze_table)
    m.solve_maze()