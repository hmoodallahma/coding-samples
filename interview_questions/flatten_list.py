#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 08:47:16 2021

@author: aisha
"""
"""Flattens a list containing unknown number of nested lists"""



"""flattens elements recursively by returning the element or else flattening
    the first element in the list"""
def flatten_list(item):
    if item == []:
        return item
    if type(item[0]) == list:
        return flatten_list(item[0]) + flatten_list(item[1:])
    return [item[0]] + flatten_list(item[1:])


    
"""flatten the list by converting the list to a string and removing all
   brackets"""
def string_flatten(item):
    flat_list = re.sub('[ \[\]]','',str(item)).split(',')
    return [int(x) for x in flat_list]        
                

a = [[1],[2,3],4,5,[6,[7,[8]]]]
b = [1,2,3,4,5,6]
c = [[[[[[[[]]]]]]],[[[]]],1]
d = [[[[[[[[]]]]]]],[[[]]]]       

"""Tests
def test_flatten_list():
    assert flatten_list(a) == [1, 2, 3, 4, 5, 6, 7, 8]
    assert flatten_list(b) == b
    assert flatten_list(c) == [1]
    assert flatten_list(d) == []

def test_string_flatten():
    assert flatten_list(a) == [1, 2, 3, 4, 5, 6, 7, 8]
    assert flatten_list(b) == b
    assert flatten_list(c) == [1]
    assert flatten_list(d) == []
"""


