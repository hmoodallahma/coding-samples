#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 14:23:26 2021

@author: aisha
"""

import collections
import random

"""Create a deck of 52 cards and shuffle them into a random order"""

Card = collections.namedtuple('Card', ['suit', 'rank'])

class CardDeck:
    
    suits = 'hearts clubs diamonds spades'.split()
    ranks = [str(n) for n in range(2, 11)] + list('JQKA')
    
    def __init__(self):
        self._cards = [Card(suit, rank) for rank in self.ranks 
                                        for suit in self.suits]
    
    def __len__(self):
        return  len(self._cards)
    
    def __getitem__(self, position):
        return self._cards[position]
    
    def choice(self,deck):
        return random.choice(deck)
    
    def shuffle(self):
        unshuffled = self._cards.copy()
        shuffled = []
        while len(unshuffled) > 0:
            x = self.choice(unshuffled)
            shuffled.append(x)
            unshuffled.remove(x)
        
        return shuffled
            


c = CardDeck()
print(c.shuffle())