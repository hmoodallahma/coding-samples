#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 10:30:08 2021

@author: aisha
"""

"""Create a fencing pool give a number of players and the outcomes of matches"""
"""assume double hits do not count and ties are allowed"""
import random

class Player:
    def __init__(self, name, player_id=0):
        self.name = name
        self.id = player_id
        # self.rank=0
        # self.hits_for=hits_for
        # self.hits_against=hits_against
        # self.net_hits = 0
        # self.match_score = 0
        """todo: abstract net_hits, hits_for, hits_against to poolresultplayer"""
         
    def set_id(self, player_id):
        self.id = player_id
        return None
    
    def __repr__(self):
        return str(self.__dict__)
   

    

"""Todo: abstract match details for player to PlayerMatch"""
class PlayerMatch(Player):
    def __init__(self, player=None, name='',player_id=0, opponent, hits_for=0, hits_against=0):
        if player:
            super().__init__(player.name, player.id)
            super().set_id(player.id)
        else:
            super().__init__(name, player_id)
            

        self.opponent = opponent
        self.hits_for = hits_for
        self.hits_against = hits_against
        self.match_score = 0
    
    def __repr__(self):
        return self.id

    def __sub__(self, other):
        return self.id - other
    
    def update_hits(self, hits_for, hits_against):

        self.hits_for= self.hits_for + hits_for
        self.hits_against =  self.hits_against +  hits_against
        self.net_hits = self.hits_for - self.hits_against
        return None     
    
    def touche(self, score=1):
        self.match_score += score
        return None

class Match:
    def __init__(self, player1, player2):
        self.player1 = PlayerMatch(player1.id, player1.name, player2.id)
        self.player2 = PlayerMatch(player2.id, player1.name, player1.id)
        self.winner, self.loser = None, None
        self.scores = [0,0]
        self.players = [self.player1, self.player2]
        



    def __repr__(self):
        """say who won first"""
        return '{} vs {}: outcome {}-{}'.format(self.winner, self.loser,
                                                self.winner.match_score,
                                                self.loser.match_score)
    def hit(self):
        """randomly assign hit"""
        hitter= random.choice(self.players)
        hitter.touche()
        return None
    
    def determine_winner(self):
        """ the player with the highest score is the winner"""
        self.winner = self.players[self.scores.index(max(self.scores))]
        return None
    
    def aller(self):
        """matches go up to 5 points"""
        while (max(self.scores) < 5):
            self.hit()
            self.scores = [player.match_score for player in self.players]
            
        self.determine_winner()
        
        for player in self.players:
            player.update_hits(*self.scores)
            self.scores.reverse()
            if player != self.winner:
                self.loser = player
        print(self)
        return self.return_match_result(self.winner,self.loser)
    
    def return_match_result(self, winner, loser):
        
        return MatchOutcome(winner.id, loser.id,
                            winner.match_score,
                            loser.match_score)
    
from collections import namedtuple

MatchOutcome = namedtuple('MatchOutcome',
     ['winner_id', 'loser_id', 'winner_score', 'loser_score'])

import operator

class Pool:
    """players will face eachother once time in each pool"""
    def __init__(self, players):
        self.players = players
        self.ranked_players = []
        self.top_three = []
        self.bottom_three = []
        self.matches = []
        self.played_matches = []
        self.player_dict = {}

        self.setUp()
    
    def setUp(self):
        self.design_pool()
        self.order_matches(self.list_matches())


    def possible_matches(self):
        """players play eachother only once"""
        n = len(self.players)   
        return n * (n-1) // 2
    
    def randomize_order(self):
        temp_dict = self.players.copy()
        self.players = []
        while len(temp_dict) > 0:
            mv_player = random.choice(temp_dict)
            self.players.append(mv_player)
            temp_dict.remove(mv_player)
        
    def design_pool(self):
        self.randomize_order()
        self.player_dict = {i+1:p for i,p in enumerate(self.players)}
        #[p.set_id(i+1) for i,p in enumerate(self.players)]
        return None
    
    def create_match_matrix(self):
         l = len(self.players)
         m = [['X'] + [1] * (l-1)]
         for i in range(1,l):
            m.append([])
            for j in range(l):
                if not i == j and m[i-1][j]:
                    m[i].append(1)
                elif i==j:
                     m[i].append('X')
                else:
                    m[i].append(0)
         return m
    
    def list_matches(self):
        match_list = []
        m = self.create_match_matrix()
        for i, v in enumerate(m):
            for j, w in enumerate(v):
                if w == 1:
                    match_list.append([i+1,j+1])
        return match_list
    

    def check_last_match(self,last_match, new_match):
        """check if players played in the previous match"""
        return len(set(last_match + new_match)) == len(last_match + new_match)
        
    
    
    def order_matches(self, matches):
        """order the matches so players can avoid playing back-to-back"""
        self.matches, temp_matches = matches[:1], matches[1:]
        possible_matches = self.possible_matches()
        old = self.matches[0]
        while len(self.matches) < possible_matches:
            for m in temp_matches:  
                no_repeats = self.check_last_match(old, m)
                tries = 0
                while not no_repeats and tries < 10:
                    m = random.choice(temp_matches)
                    no_repeats = self.check_last_match(old, m)
                    tries += 1
                
                self.matches.append(m)
                temp_matches.remove(m)
                old = m
        #Todo: output as a table
        return None
    #Todo: break a list of players into multiple pools of similar size
    # using recursion

class PoolResult:
    def __init__(self, pool):
        self.pool = pool
        self.results = []
        self.get_results()
        self.rank = []
    
    def get_results(self):
       """mocking play"""
       for m in self.pool.matches:
            p1, p2 = [self.pool.player_dict[k] for k in m]
            played_match = Match(p1, p2)
            match_result = played_match.aller()
            self.results.append(match_result)
    
    def rank_by_wins(self):
        #Todo: Rank results by wins
        return None
    
    def rank_by_net_hits(self):
        #Todo: Rank results by net hits
        """calculate rank by net hits in case of ties"""
        return None

    def create_result_matrix(self):
        matches = self.pool.create_match_matrix()
        
        for r in self.results:
            print(type(r.winner_id))
            matches[r.winner_id-1][r.loser_id-1] = r.winner_score
            matches[r.loser_id-1][r.winner_id-1] = r.loser_score
        return matches

    def print_result_matrix(self):
        matches = self.create_result_matrix()
        top_row, table = '  ',''
        for e, m in enumerate(matches):
            top_row += ' {} '.format(e + 1)
            t = ''
            for i in m:
                t += ' {} '.format(i)
            table += '{} {}\n'.format(e+1, t)
        print(top_row)
        print(table)
        """example output
               1  2  3  4  5  6  7 
            1  X  3  1  5  3  5  5 
            2  5  X  2  5  5  5  2 
            3  1  5  X  5  1  3  2 
            4  4  1  3  X  3  1  1 
            5  5  3  1  5  X  1  1 
            6  3  3  5  1  1  X  4 
            7  2  5  5  1  5  5  X  """
        """todo: add rank to table and calculate net hits"""
        """output pretty version of table with names and results"""
        
            

if __name__ == '__main__':
    player_names = ['patrick', 'aleks', 'john', 'adrian', 'andrew', 'aisha', 'bob']
    players = [Player(n) for i, n in enumerate(player_names)] 
    p = Pool(players)
    p.matches
    pr= PoolResult(p)
    #x = p.list_matches()
    pr.print_result_matrix()
                    


