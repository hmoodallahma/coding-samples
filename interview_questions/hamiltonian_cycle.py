#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 15:24:05 2021

@author: aisha
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:51:25 2021

@author: aisha
"""

class HamiltonianCycle:
    
    def __init__(self, adjacency_matrix):
        self.n = len(adjacency_matrix)
        self.adjacency_matrix = adjacency_matrix
        self.path = []
        
    
    def hamiltonian_cycle(self):
        
        self.path.append(0)
        
        if self.solve(1):
            self.solve_cycle()
        else:
            print('There is no solution to the problem')
    
    def solve(self, position):
        #make sure the last element connects with the first element
        if position == self.n:
            last_item_index = position
            if self.adjacency_matrix[self.path[-1]][0] == 1:
                self.path.append(0)
                return True
    
        for vertex_index in range(1, self.n):
            if self.is_feasible(vertex_index, position):
                #include vertex with with vertex index in solution
                self.path.append(vertex_index)
                #try to include next vertex in solution
                if self.solve(position+1):
                    return True
                else:
                    #we have to backtrack
                    #we have to remove the last item from path
                    self.path.pop()
        #we have now considered all vertices without success
        return False
    
    def is_feasible(self, vertex, actual_position):
        #need to check whether two vertices are connected to eachother
        #two vertices are connected if they are in adjacency_matrix

        #indices start at 0
        #check if there connection between the two vertices
        if self.adjacency_matrix[self.path[actual_position-1]][vertex] == 0:
            #no connection
            return False
        #also need to be sure not to revisit vertices unless its the first one
        for i in range(actual_position):
            if self.path[i] == vertex:
                return False
        
        return True
                
                
    def solve_cycle(self):
        for v in self.path:
            print(v)
        
        
if __name__ == '__main__':
    m = [[0, 1, 1],
         [1, 0, 1],
         [1, 1, 0]]
    hp = HamiltonianCycle(m)
    hp.hamiltonian_cycle()
        
        