#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 09:57:20 2021

@author: aisha
"""

"""Hamiltonian cycle of knights on a chess board of M x N size
   If always feasible unless:
   - M and N are odd numbers
   - M = 1,2,4 or 
   - M = 3 and N = 4,6,8
"""

class KnightsTour:
    
    def __init__(self, board_size):
        self.board_size = board_size
        #Possible horizontal components of moves
        #right is positive left is negative
        self.x_moves = [2,1,-1,-2,-2,-1,1,2]
        #up is positive down is negative
        self.y_moves = [1, 2,2,1,-1,-2,-2,-1]        
        self.solution_matrix = [[-1 for i in range(self.board_size)] 
                                for n in range(self.board_size)]
        self.solve_problem()
    
    def solve_problem(self):
        #start with the top left cell
        self.solution_matrix[0][0] = 0
        
        if self.solve(1,0,0):
            return self.print_solution()
        
        else:
            print('There is no solution to the problem')
            
    
    def solve(self, counter, x_position, y_position):
        
        if counter == self.board_size ** 2:
            return True
        
        for move_index in range(len(self.x_moves)):
            #update the move
            new_x = x_position + self.x_moves[move_index]
            new_y = y_position + self.y_moves[move_index]
            
            if self.is_feasible(new_x, new_y):
                self.solution_matrix[new_x][new_y] = counter
                if self.solve(counter+1, new_x, new_y):
                    return True
                else:
                    #backtracking    
                    self.solution_matrix[new_x][new_y] = -1
        #tried all the moves
        return False
    
    def is_feasible(self, x_position, y_position):
        
        if x_position < 0 or x_position >= self.board_size:
            return False
        
        if y_position < 0 or y_position >= self.board_size:
            return False
        
        if self.solution_matrix[x_position][y_position] > -1:
            return False
        
        return True
            
    
    def print_solution(self):
        for i in range(self.board_size):
            for j in range(self.board_size):
                print('[{}]'.format(self.solution_matrix[i][j]) , end='')
            print('\n')
            


if __name__ == '__main__':
    k = KnightsTour(5)