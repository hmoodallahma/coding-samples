#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 13:10:28 2021

@author: aisha
"""

"""A Class to solve colouring problems:
    Assign colours to vertices of a graph such that no two vertices are the
    same colour"""
class ColouringProblem:
    
    def __init__(self, adjacency_matrix):
        #The number of vertices is equal to the length of the adjacency matrix
        self.n = len(adjacency_matrix)
        self.adjacency_matrix = adjacency_matrix
        self.num_colours = 1
        self.colours = [0 for _ in range(self.n)]
        #Todo: determine actual colour codes
    
    
    def solve_colouring(self):
        #determine the minimum numner of colours required
        while self.colouring_problem() == False:
            self.num_colours += 1
            self.colouring_problem()
       
        self.show_result()
        
    def colouring_problem(self):
        #We call solve with first index 0
        
        if self.solve(0):
            return True
        else:
            return False
    
    def solve(self, node_index):
        #All nodes coloured, problem is solved
        if node_index == self.n:
            return True
        
        #Consider the colours
        for colour_index in range(1, self.num_colours + 1):
            if self.is_colour_valid(node_index, colour_index):
                self.colours[node_index] = colour_index
                if self.solve(node_index + 1):
                    return True
                
                #backtracking - do nothing
        #Tried to assign value but not possible
        return False
    
    def is_colour_valid(self, node_index, colour_index):
        #check nodes are connected
        #check colour is not shared with adjacent nodes
        for i in range(self.n):
            if self.adjacency_matrix[node_index][i] == 1 and \
                colour_index == self.colours[i]:
                return False
        return True
    
    def show_result(self):
        print('The minimum number of colours is {}'.format(self.num_colours))
        for v, c in zip(range(self.n), self.colours):
            print('Node %d has colour %d' % (v,c))
        
    
    
    
if __name__ == '__main__':
    m = [[0,1,1,1],
         [1,0,1,0],
         [1,1,0,1],
         [1,0,1,0]]
    
    
    p = ColouringProblem(m)
    p.solve_colouring()
                
                