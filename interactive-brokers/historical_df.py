from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract
import threading
import time
import pandas as pd
"""Returns a dataframe of historical data for supplied tickers and specified duration"""
class TradingApp(EWrapper, EClient):
    def __init__(self):
        EClient.__init__(self,self)
        self.data = {}
        
    def error(self, reqId, errorCode, errorString):
        print("Error {} {} {}".format(reqId,errorCode,errorString))

    def contractDetails(self, reqId, contractDetails):
        print("reqId: {}; contractDetails: {} ".format(reqId, contractDetails))

    def historicalData(self, reqId, bar):
        if not reqId in self.data:
            self.data[reqId] = [{"date": bar.date, "open": bar.open, "low": bar.low, "close": bar.close, "volume": bar.volume}]
        else:
            self.data[reqId].append({"date": bar.date, "open": bar.open, "low": bar.low, "close": bar.close, "volume": bar.volume})            
        #print("HistoricalData. ReqId:", reqId, "BarData.", bar)
        print("date: {} open {} low {} close {} high {} volume {}".format(bar.date, bar.open, bar.low, bar.close, bar.high, bar.volume))

 
def websocket_con():
    app.run()
    #event.wait()
    # if event.is_set:
    #     app.close()
        

def usTechStk(symbol, sec_type="STK", currency="USD",exchange="ISLAND"):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = sec_type
    contract.currency = currency
    contract.exchange = exchange
    return contract

def histData(req_num, contract, duration='1 M', candle='15 mins'):
    app.reqHistoricalData(reqId=req_num,
                      contract=contract,
                      endDateTime='',
                      durationStr=duration,
                      barSizeSetting=candle,
                      whatToShow='ADJUSTED_LAST',
                      useRTH=1,
                      formatDate=1,
                      keepUpToDate=0,
                      chartOptions=[] )	
    

event = threading.Event()
app = TradingApp()      
app.connect("127.0.0.1", 7497, clientId=1)


con_thread = threading.Thread(target=websocket_con)
con_thread.start()
time.sleep(1)




def ohlcDataFrame(tradeapp_obj, tickers):    
    df_dict = {}
    
    for ticker in tickers:
        df_dict[ticker] = pd.DataFrame(tradeapp_obj.data[tickers.index(ticker)])
        df_dict[ticker].set_index("date")
    
    return df_dict

tickers = ["FB", "INTC", "AMZN"]

## want to run code for a certain amount of time
start_time = time.time()
end_time = start_time + 60 * 60



while time.time() <= end_time:
    for ticker in tickers:
        histData(tickers.index(ticker), usTechStk(ticker), '3600 S', '30 secs')
        print("#################{}##########".format(ticker))
        time.sleep(3)
    print("{} ".format(time.time()-start_time))
    hist_data = ohlcDataFrame(app, tickers)
    print(hist_data)
    time.sleep(30 - ((time.time()-start_time)%30))
    





#event.set()