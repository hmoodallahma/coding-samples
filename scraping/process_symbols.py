import pandas as pd
from dirty.models import *
import ast, re
from fuzzywuzzy import fuzz, process


def get_symbols_db_df():
	symbols = Symbol.objects.all()
	data = []
	for s in symbols:
		s = s.__dict__
		s['meta'] = ast.literal_eval(s['meta'])
		s = {**s, **s['meta']}
		data.append(s)
	return pd.DataFrame(data)

def get_stocks_db_df():
	stocks = Stock.objects.all()
	return pd.DataFrame(list(stocks))

def get_notins():
	stockos = pd.read_csv('/Users/aisha/documents/thombert_etfs/stockos.csv')

def check_first_word_matches(word, comparer):
	name = name.split(' ')[0]
	return preg_match(name, comparer)

scorer_dict = { 'R':fuzz.ratio, 
                'PR': fuzz.partial_ratio, 
                'TSeR': fuzz.token_set_ratio, 
                'TSoR': fuzz.token_sort_ratio,
                'PTSeR': fuzz.partial_token_set_ratio, 
                'PTSoR': fuzz.partial_token_sort_ratio, 
                'WR': fuzz.WRatio, 
                'QR': fuzz.QRatio,
                'UWR': fuzz.UWRatio, 
                'UQR': fuzz.UQRatio }

def get_best_match_df(good_names):
	y = []
	for name in names: 
		x = process.extractBests(name, good_names, scorer=fuzz.partial_ratio, limit=2, score_cutoff=90) 
		if x: 
			print(x[0])
			g = df.loc[df['Company'].isin(x[:][0])]
			g['title'] = name
			y.append(g)
			print(g)
	return pd.concat(y)

def save_renames(good_names):
	df = get_best_match_df(good_names)
	df.to_csv('/Users/aisha/documents/thombert_etfs/possible_stock_renames.csv')
	
def select_nouns(name,divs):
	nouns = []
	for div in divs:
		text = div.text 
		doc = nlp(text) 
		for entity in doc.ents: 
			if (fuzz.ratio(name.title(), entity.text) > 87  or fuzz.partial_ratio(name, entity.text) > 87) and entity.label_ == 'ORG': 
				if 'QuotesSymbolLAST_PRICEChange%' not in entity.text and len(re.findall(r'\W{3,}',entity.text)) is 0:
					nouns.append(entity.text)
	nouns = list(dict.fromkeys(nouns))
	return nouns

import spacy
nlp = spacy.load("en_core_web_sm")
def digest_divs(divs):
	for div in divs: 
		text = div.text 
		doc = nlp(text) 
		print("Noun phrases:", [chunk.text for chunk in doc.noun_chunks]) 