from dirty.etfs.utils.browser import *
from dirty.etfs.utils.soup import *
import pandas as pd
import numpy as np


def get_filings_table_with_links(url):
	browser = setup_browser()
	browser.get(url)
	soup = get_soup(browser)
	t = soup.find_all('table')[2]
	links =  [ link['href'] for link in list(filter(lambda x: x.text == '\xa0Documents', t.find_all('a')))]
	df = pd.read_html(str(t))[0]
	df['link'] = links
	form_8 = get_form_8(df)
	dfs = []
	for i, row in form_8.iterrows():
		try:
			#get label data
			dfs.append(browse_to_form(row['link'], browser))
			#get text data
		except:
			print(row)
			pass
	return pd.concat(dfs)
	


def get_form_10K(df):
	return df[df['Filings'] == '10-K']


	
def browse_to_form(link, browser):
	browser.get(f'https://www.sec.gov/{link}')
	soup = get_soup(browser)

	t = soup.find_all('table')[1]
	links = [ url['href'] for url in list(filter(lambda x: 'htm.xml' in x.text, t.find_all('a')))]
	links2 = [ url['href'] for url in list(filter(lambda x: 'htm' in x.text, t.find_all('a')))]
	if not len(links):
		links = [ url['href'] for url in list(filter(lambda x: 'html' in x.text, t.find_all('a')))]
	try:
		return read_form(f'https://www.sec.gov/{links[0]}', browser)
	except:
		pass


def read_form(url, browser):
	browser.get(url)
	soup = get_xsoup(browser)
	xd = format_xml_df(soup)
	return pd.DataFrame(xd)
	

def format_xml_df(soup):
	labels = [
				 'EntityRegistrantName',
				 'EntityIncorporationStateCountryCode',
				 'EntityFileNumber',
				 'EntityTaxIdentificationNumber',
				 'EntityAddressAddressLine1',
				 'EntityAddressAddressLine2',
				 'EntityAddressCityOrTown',
				 'EntityAddressStateOrProvince',
				 'EntityAddressPostalZipCode',
				 'CityAreaCode',
				 'LocalPhoneNumber',
				 'WrittenCommunications',
				 'SolicitingMaterial',
				 'PreCommencementTenderOffer',
				 'PreCommencementIssuerTenderOffer',
				 'Security12bTitle',
				 'TradingSymbol',
				 'SecurityExchangeName',
				 'EntityEmergingGrowthCompany']
	xdict = {}
	for label in labels:
		vals = soup.find_all(f'dei:{label}')
		xdict[label] = [x.text.strip() for x in vals]
	return xdict

flatten = lambda l: [item for sublist in l for item in sublist]

def GetMaxFlow(flows):        
    maks=max(flows, key=lambda k: len(flows[k]))
    return len(flows[maks])

def process_footnotes(d):
	d['footnotes'] = [';'.join(d['footnotes'])]

def dict_to_df(d):
	max_len = GetMaxFlow(d)
	process_footnotes(d)
	for k,v in d.items():
		if len(v) < max_len:
			if len(v) == 0:
				d[k] = np.repeat('', max_len)
			elif k in ['rptOwnerName', 'remarks', 'officerTitle','footnotes']:
				d[k] = np.repeat(d[k], max_len)
			else:
				d[k] = [*d[k], *np.repeat('', max_len-len(d[k]))]
	try:
		return pd.DataFrame(d)
	except:
		print(d)
		pass




x = 'https://www.sec.gov/Archives/edgar/data/1525321/000156218020004596/xslF345X03/primarydocument.xml'